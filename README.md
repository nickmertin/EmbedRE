# EmbedRE

A C11 preprocessor library to embed regular expressions in source code

## API

To generate code:
1. Define `RE_MODE` containing the requested mode (or multiple modes bitwise ORed together)
2. Define `RE_NAME` containing the name of the generated element (a suffix will be added for each mode if multiple modes specified)
3. Define `RE_DECL` to generate declaration only (function modes only) OR define RE containing the regular expression in EmbedRE flavour
4. Include `impl.h`

Example:

    #define RE_MODE RE_BRE | RE_MATCH
    #define RE_NAME is_phone
    #define RE START CHAR("(") NUM_EXACT(CLASS(DIGIT), 3) CHAR(")") CHAR(" ") NUM_EXACT(CLASS(DIGIT), 3) NUM_RANGE(CHAR("-"), 0, 1) NUM_EXACT(CLASS(DIGIT), 4) END

    #include "../src/impl.h"

This will create a constant named `is_phone_bre` of type `const char[]`, and a function with the prototype `bool is_phone_match(const char *);`.

## EmbedRE Regex Flavour Syntax

Technically, this conforms to the defenition of a regular expression. Don't argue it. The following features are supported:

Feature|EmbedRE syntax|POSIX BRE syntax
-------|--------------|----------------
Start anchor|`START`|`^`
End anchor|`END`|`$`
Exact character `a`|`CHAR("a")`|`a`
Character class `class`|`CLASS(class)`|`[:class:]`
`expr` recurs `n` to `m` times|`NUM_RANGE(expr, n, m)`|`expr\{n,m\}`
`expr` recurs exactly `n` times|`NUM_EXACT(expr, n)`|`expr\{n\}`
`expr` recurs at least `n` times|`NUM_ATLEAST(expr, n)`|`expr\{n,\}`
A character that is either `a`, `b`, or in the class `class`|`OPTS(CHAR_OPT("a") CHAR_OPT("b") CLASS_OPT(class))`|`[ab[:class:]]`
A subexpression `expr`|`GROUP(expr)`|`(expr)`
Subexpression #`n`|`REF(n)`|`\n`
Any character|`ANY`|`.`

Within an EmbedRE regular expression, whitespace (other than that between the quotes in a CHAR or CHAR_OPT entity) is ignored.

## Character classes

All POSIX BRE character classes are supported, with the names capitalized:

EmbedRE syntax|POSIX BRE syntax|meaning
------------|--------------|-------
`CLASS(UPPER)`|`[:upper:]`|Uppercase letters
`CLASS(LOWER)`|`[:lower:]`|Lowercase letters
`CLASS(ALPHA)`|`[:alpha:]`|Upper- and lowercase letters
`CLASS(DIGIT)`|`[:digit:]`|Digits
`CLASS(XDIGIT)`|`[:xdigit:]`|Hexadecimal digits
`CLASS(ALNUM)`|`[:alnum:]`|Digits, upper- and lowercase letters
`CLASS(PUNCT)`|`[:punct:]`|Punctuation (all graphic characters except letters and digits)
`CLASS(BLANK)`|`[:blank:]`|Space and TAB characters only
`CLASS(SPACE)`|`[:space:]`|Blank (whitespace) characters
`CLASS(CNTRL)`|`[:cntrl:]`|Control characters
`CLASS(GRAPH)`|`[:graph:]`|Graphic characters (all characters which have graphic representation)
`CLASS(PRINT)`|`[:print:]`|Graphic characters and space
