#if defined(RE_MODE) && defined(RE_NAME) && (defined(RE_DECL) || defined(RE))


#include "include.h"


#ifndef RE_CTYPE
#define RE_CTYPE char
#endif

#ifndef RE_ATTR
#define RE_ATTR
#endif

//TODO: create function

#define __NAME_E(name) __NAME(name)

#if (RE_MODE) & RE_BRE != 0

#define __MODE RE_BRE

#if (RE_MODE) == RE_BRE
#define __NAME(name) name
#else
#define __NAME(name) name##_bre
#endif

const char __NAME_E(RE_NAME)[] =

#include "eval.h"

        ;

#undef __MODE
#undef __NAME

#endif

#if (RE_MODE) & RE_MATCH != 0

#define __MODE RE_MATCH

#if (RE_MODE) == RE_MATCH
#define __NAME(name) name
#else
#define __NAME(name) name##_match
#endif

#ifdef RE_DECL

RE_ATTR bool __NAME(RE_NAME)(const RE_CTYPE *str);

#else

RE_ATTR bool __NAME_E(RE_NAME)(const RE_CTYPE *str) {
    size_t start = 0, i, l = strlen(str), g = 0;
    bool s = false, f = true;
    struct re_group_t groups[9];

#define __YES return true;

#include "eval.h"

    return false;
}

#undef __MODE
#undef __NAME

#endif

#endif

#if (RE_MODE) & RE_FIND

#define __MODE RE_FIND

#if (RE_MODE) == RE_FIND
#define __NAME(name) name
#else
#define __NAME(name) name##_find
#endif

#ifdef RE_DECL

RE_ATTR size_t __NAME_E(RE_NAME)(const RE_CTYPE *str);

#else

RE_ATTR size_t __NAME_E(RE_NAME)(const RE_CTYPE *str) {
    size_t start = 0, i, l = strlen(str), g = 0;
    bool s = false, f = true;
    struct re_group_t groups[9];

#define __YES return start;

#include "eval.h"

    return (size_t) -1;
}

#undef __MODE
#undef __NAME

#endif

#endif

#undef __NAME_E

#undef RE_CTYPE
#undef RE_ATTR
#undef RE_NAME
#undef RE_MODE

#ifndef RE_DECL
#undef RE
#endif


#endif
