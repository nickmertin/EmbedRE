#ifdef DEBUG
#define __DEBUG(...) __VA_ARGS__
#else
#define __DEBUG(...)
#endif

#if __MODE == RE_BRE

#define __CLASS_UPPER "[:upper:]"
#define __CLASS_LOWER "[:lower:]"
#define __CLASS_ALPHA "[:alpha:]"
#define __CLASS_DIGIT "[:digit:]"
#define __CLASS_XDIGIT "[:xdigit:]"
#define __CLASS_ALNUM "[:alnum:]"
#define __CLASS_PUNCT "[:punct:]"
#define __CLASS_BLANK "[:blank:]"
#define __CLASS_SPACE "[:space:]"
#define __CLASS_CNTRL "[:cntrl:]"
#define __CLASS_GRAPH "[:graph:]"
#define __CLASS_PRINT "[:print:]"

#define START "^"
#define END "$"
#define CHAR(c) c
#define CLASS(c) __CLASS_##c
#define NUM_RANGE(re, n, m) re "\\{" #n "," #m "\\}"
#define NUM_EXACT(re, n) re "\\{" #n "\\}"
#define NUM_ATLEAST(re, n) re "\\{" #n ",\\}"
#define OPTS(re) "[" re "]"
#define CHAR_OPT(c) c
#define CLASS_OPT(c) __CLASS_##c
#define GROUP(re) "\\(" re "\\)"
#define REF(n) "\\" #n
#define ANY "."

RE

#elif __MODE == RE_MATCH || __MODE == RE_FIND

#define __SECTION for (size_t _i = i, r = 1; r; r = 0, __DEBUG(printf("RESET %u -> %u\n", (unsigned) i, (unsigned) _i),) i = _i)
#define __SINGLE(cond) __SECTION if (!(s || cond)) { __DEBUG(printf("'%s': %c @ %u\n", #cond, str[i], (unsigned) i);) continue; } else
#define __CLASS(c) re_classes[RE_##c][str[i] >= 0x40] >> (str[i] % 0x40) & 1
#define __RANGE(cond, n, re, ...) for (size_t j = 0, _i = i, rr = 1, ri = i; j == n ? s = 1 : 0, cond && rr; s = 0, (--rr || j < n || i >= l) && (i = ri) || rr|| (i = _i)) re if (rr = 2, ri = i, ++j < n) { __DEBUG(printf(__VA_ARGS__);) continue; } else if (!(s = 0))

#define START if (f = 0, 1)
#define END if (i == l)
#define CHAR(c) __SINGLE(str[i++] == *c)
#define CLASS(c) __SINGLE(__CLASS(c) && ++i)
#define NUM_RANGE(re, n, m) __RANGE(j <= m, n, re, "NUM_RANGE for '%s': %c - %u of [%u,%u] @ %u\n", #re, str[i], (unsigned) j, (unsigned) n, (unsigned) m, (unsigned) i)
#define NUM_EXACT(re, n) NUM_RANGE(re, n, n)
#define NUM_ATLEAST(re, n) __RANGE(1, n, re, "NUM_ATLEAST for '%s': %c - %u of %u @ %u\n", #re, str[i], (unsigned) j, (unsigned) n, (unsigned) i)
#define OPTS(re) __SINGLE((0 re) && r && ++i)
#define CHAR_OPT(c) || str[i] == *c
#define CLASS_OPT(c) || __CLASS(c)
#define GROUP(re) for (size_t _g = g++, gi = groups[_g].start = i, r = 1; r; --r, --g) re if (groups[_g].length = i - gi, 1)
#define REF(n) __SINGLE((s = 0, !memcmp(str + i, str + groups[n - 1].start, groups[n - 1].length) && (i += groups[n - 1].length)))
#define ANY __SINGLE(i++ < l)

for (; i = start, f && start < l; ++start) RE __YES

#undef __YES
#undef __SECTION
#undef __SINGLE
#undef __CLASS
#undef __RANGE

#endif

#undef START
#undef END
#undef CHAR
#undef CLASS
#undef NUM_RANGE
#undef NUM_EXACT
#undef NUM_ATLEAST
#undef OPTS
#undef CHAR_OPT
#undef CLASS_OPT
#undef GROUP
#undef REF
#undef ANY
