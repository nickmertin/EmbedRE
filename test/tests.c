#include <stdio.h>
#include "../src/include.h"


#define DEBUG


#define RE_MODE RE_BRE | RE_MATCH
#define RE_NAME is_phone
#define RE START CHAR("(") NUM_EXACT(CLASS(DIGIT), 3) CHAR(")") CHAR(" ") NUM_EXACT(CLASS(DIGIT), 3) NUM_RANGE(CHAR("-"), 0, 1) NUM_EXACT(CLASS(DIGIT), 4) END

#include "../src/impl.h"


#define RE_MODE RE_BRE | RE_MATCH
#define RE_NAME is_token
#define RE START OPTS(CLASS_OPT(ALPHA) CHAR_OPT("_")) NUM_ATLEAST(OPTS(CLASS_OPT(ALNUM) CHAR_OPT("_")), 0) END

#include "../src/impl.h"


#define RE_MODE RE_BRE | RE_FIND
#define RE_NAME has_var
#define RE OPTS(CLASS_OPT(ALPHA) CHAR_OPT("_") CHAR_OPT("$")) NUM_ATLEAST(OPTS(CLASS_OPT(ALNUM) CHAR_OPT("_") CHAR_OPT("$")), 0)

#include "../src/impl.h"


#define RE_MODE RE_BRE | RE_MATCH
#define RE_NAME is_xml_tag
#define RE START CHAR("<") GROUP(OPTS(CLASS_OPT(ALPHA) CHAR_OPT("_")) NUM_ATLEAST(OPTS(CLASS_OPT(ALNUM) CHAR_OPT("_") CHAR_OPT("-") CHAR_OPT(".")), 0)) CHAR(">") NUM_ATLEAST(ANY, 0) CHAR("<") CHAR("/") REF(1) CHAR(">") END

#include "../src/impl.h"


int main() {
    printf("Phone: %s\n", is_phone_bre);
    printf("Token: %s\n", is_token_bre);
    printf("JavaScript: %s\n", has_var_bre);
    printf("XML: %s\n", is_xml_tag_bre);
    char input[40];
    do {
        printf("Input up to 40 characters:");
        gets(input);
        printf(is_phone_match(input) ? "%s is a phone number\n" : "%s is not a phone number\n", input);
        printf(is_token_match(input) ? "%s is a token\n" : "%s is not a token\n", input);
        size_t i = has_var_find(input);
        if (i == (size_t) -1)
            printf("%s does not have a JavaScript variable name\n", input);
        else
            printf("%s has a Javascript variable name starting at index %u\n", input, (unsigned) i);
        printf(is_xml_tag_match(input) ? "%s is an XML tag\n" : "%s is not an XML tag\n", input);
    } while (input[0]);
}
